﻿using System;
using System.Collections.Generic;
using DAL;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.SqlServerCe;

namespace Logging
{
    public class clsLogging
    {
        public void WriteLog(Exception ex)
        {
            SQLHelper sqlHelper = new SQLHelper();
            List<SqlCeParameter> parameters = new List<SqlCeParameter>();
            parameters.Add(new SqlCeParameter("_DateTime", DateTime.Now));
            parameters.Add(new SqlCeParameter("_ErrorMessage", ex.Message));
            parameters.Add(new SqlCeParameter("_ErrorStack", ex.StackTrace));

            //sqlHelper.executeSP<int>(parameters, "InsertLog");
        }
    }
}
