﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Test.Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div style="font-weight:bold">Search Title</div>
    <span style="height:20px; vertical-align:top">Title Name :</span>
    <asp:TextBox ID="txtSearchName" runat="server"></asp:TextBox>
    <asp:Button ID="btnSearch" runat="server" Text="Search" 
        onclick="btnSearch_Click" />

   <div>
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
               <asp:GridView ID="GridView1" runat="server" 
                        Width="940px"  HorizontalAlign="Center"
                        OnRowCommand="GridView1_RowCommand" 
                        AutoGenerateColumns="false" AllowPaging="true" PageSize="10" PageIndex="0"
                        CellPadding="4"
                        DataKeyNames="TitleId" 
                        OnPageIndexChanging ="GridView1_PageIndexChanging"
                        CssClass="table table-hover table-striped">
                <Columns>
                   <asp:ButtonField CommandName="detail" 
                         ControlStyle-CssClass="btn btn-info" ButtonType="Button" 
                         Text="Detail" HeaderText="Detailed View"/>
            <asp:BoundField DataField="TitleId" HeaderText="Title Id" />
            <asp:BoundField DataField="TitleName" HeaderText="Tite name" />
            <asp:BoundField DataField="TitleNameSortable" HeaderText="TitleNameSortable" />
            <asp:BoundField DataField="TitleTypeId" HeaderText="Title Type Id" />
            <asp:BoundField DataField="ReleaseYear" HeaderText="Release Year" />
            <asp:BoundField DataField="ProcessedDateTimeUTC" HeaderText="ProcessedDateTimeUTC" />
               </Columns>
               </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            

        <img src="" alt="Loading.. Please wait!"/>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="currentdetail" class="modal hide fade" tabindex=-1 role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Title Detailed View</h3>
       </div>
   <div class="modal-body">
<!-- The tab headings. -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#Award" data-toggle="tab">Award</a></li>
    <li><a href="#Participant" data-toggle="tab">Participant</a></li>
    <li><a href="#StoryLine" data-toggle="tab">StoryLine</a></li>
    <li><a href="#Genre" data-toggle="tab">Genre</a></li>
        <li><a href="#OtherName" data-toggle="tab">OtherName</a></li>
</ul>
            <!-- The tab content. -->
            <div id="my-tab-content" class="tab-content">
 
                <div class="tab-pane active" id="Award">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                                <asp:DetailsView ID="AwardDetailsView" runat="server" 
                                          CssClass="table table-bordered table-hover" 
                                           BackColor="White" ForeColor="Black"
                                           FieldHeaderStyle-Wrap="false" 
                                           FieldHeaderStyle-Font-Bold="true"  
                                           FieldHeaderStyle-BackColor="LavenderBlush" 
                                           FieldHeaderStyle-ForeColor="Black"
                                           BorderStyle="Groove" AutoGenerateRows="False">
                                    <Fields>
                             <asp:BoundField DataField="Id" HeaderText="Award Id" />
                             <asp:BoundField DataField="AwardWon" HeaderText="Award Won" />
                             <asp:BoundField DataField="AwardYear" HeaderText="Award Year" />
                             <asp:BoundField DataField="AwardName" HeaderText="Award Name" />
                             <asp:BoundField DataField="AwardCompany" HeaderText="Award Company" />
                                    </Fields>
                              </asp:DetailsView>
                       </ContentTemplate>
                       <Triggers>
                           <asp:AsyncPostBackTrigger ControlID="GridView1"  EventName="RowCommand" />  
                       </Triggers>

                       </asp:UpdatePanel>
                </div>
 
                <div class="tab-pane" id="Participant">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                                <asp:DetailsView ID="ParticipantDetailsView" runat="server" 
                                          CssClass="table table-bordered table-hover" 
                                           BackColor="White" ForeColor="Black"
                                           FieldHeaderStyle-Wrap="false" 
                                           FieldHeaderStyle-Font-Bold="true"  
                                           FieldHeaderStyle-BackColor="LavenderBlush" 
                                           FieldHeaderStyle-ForeColor="Black"
                                           BorderStyle="Groove" AutoGenerateRows="False">
                                    <Fields>
                             <asp:BoundField DataField="Id" HeaderText="Participant Id" />
                             <asp:BoundField DataField="Name" HeaderText="Name" />
                             <asp:BoundField DataField="ParticipantType" HeaderText="Participant Type" />
                             <asp:BoundField DataField="IsKey" HeaderText="IsKey" />
                             <asp:BoundField DataField="RoleType" HeaderText="Role Type" />
                             <asp:BoundField DataField="IsOnScreen" HeaderText="IsOnScreen" />
                                   </Fields>
                              </asp:DetailsView>
                       </ContentTemplate>
                       <Triggers>
                           <asp:AsyncPostBackTrigger ControlID="GridView1"  EventName="RowCommand" />  
                       </Triggers>

                       </asp:UpdatePanel>
                </div>
 
                <div class="tab-pane" id="StoryLine">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                                <asp:DetailsView ID="StoryLineDetailsView" runat="server" 
                                          CssClass="table table-bordered table-hover" 
                                           BackColor="White" ForeColor="Black"
                                           FieldHeaderStyle-Wrap="false" 
                                           FieldHeaderStyle-Font-Bold="true"  
                                           FieldHeaderStyle-BackColor="LavenderBlush" 
                                           FieldHeaderStyle-ForeColor="Black"
                                           BorderStyle="Groove" AutoGenerateRows="False">
                                    <Fields>
                             <asp:BoundField DataField="Id" HeaderText="Storyline Id" />
                             <asp:BoundField DataField="Type" HeaderText="Type" />
                             <asp:BoundField DataField="Language" HeaderText="Language" />
                             <asp:BoundField DataField="Description" HeaderText="Description" />
                                   </Fields>
                              </asp:DetailsView>
                       </ContentTemplate>
                       <Triggers>
                           <asp:AsyncPostBackTrigger ControlID="GridView1"  EventName="RowCommand" />  
                       </Triggers>

                       </asp:UpdatePanel>

                </div>

                <div class="tab-pane" id="Genre">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                                <asp:DetailsView ID="GenreDetailsView" runat="server" 
                                          CssClass="table table-bordered table-hover" 
                                           BackColor="White" ForeColor="Black"
                                           FieldHeaderStyle-Wrap="false" 
                                           FieldHeaderStyle-Font-Bold="true"  
                                           FieldHeaderStyle-BackColor="LavenderBlush" 
                                           FieldHeaderStyle-ForeColor="Black"
                                           BorderStyle="Groove" AutoGenerateRows="False">
                                    <Fields>
                             <asp:BoundField DataField="GenreId" HeaderText="Genre Id" />
                             <asp:BoundField DataField="Name" HeaderText="Genre Name" />
                                   </Fields>
                              </asp:DetailsView>
                       </ContentTemplate>
                       <Triggers>
                           <asp:AsyncPostBackTrigger ControlID="GridView1"  EventName="RowCommand" />  
                       </Triggers>

                       </asp:UpdatePanel>

                </div>

                <div class="tab-pane" id="OtherName">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                                <asp:DetailsView ID="OthernameDetailsView" runat="server" 
                                          CssClass="table table-bordered table-hover" 
                                           BackColor="White" ForeColor="Black"
                                           FieldHeaderStyle-Wrap="false" 
                                           FieldHeaderStyle-Font-Bold="true"  
                                           FieldHeaderStyle-BackColor="LavenderBlush" 
                                           FieldHeaderStyle-ForeColor="Black"
                                           BorderStyle="Groove" AutoGenerateRows="False">
                                    <Fields>
                                     <asp:BoundField DataField="TitleId" HeaderText="Title Id" />
                                     <asp:BoundField DataField="TitleNameLanguage" HeaderText="TitleName Language" />
                                     <asp:BoundField DataField="TitleNameType" HeaderText="TitleName Type" />
                                     <asp:BoundField DataField="TitleNameSortable" HeaderText="TitleName Sortable" />
                                     <asp:BoundField DataField="TitleName" HeaderText="TitleName Title Name" />
                                   </Fields>
                              </asp:DetailsView>
                       </ContentTemplate>
                       <Triggers>
                           <asp:AsyncPostBackTrigger ControlID="GridView1"  EventName="RowCommand" />  
                       </Triggers>

                       </asp:UpdatePanel>

                </div>
            </div>
                <div class="modal-footer">
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>
            </div>

    </div>
    </div>
    <hr />
</asp:Content>
