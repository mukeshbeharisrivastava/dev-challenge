﻿using System;
using System.Web.UI;
using BAL;
//using Logging;
using System.Web.UI.WebControls;
using System.Data.SqlServerCe;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
//using DAL;

namespace Test
{
    public partial class Default : System.Web.UI.Page
    {
        DataTable dataTable;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Populate the GridView.
                BindGridView(null);
            }
        }


        private void BindGridView(string titleName)
        {
            try
            {
                BALTitle balTitle = new BALTitle();
                GridView1.DataSource = balTitle.getTitles(titleName);
                GridView1.DataBind();
            }
            catch (Exception ex)
            {
                //clsLogging logError = new clsLogging();
                //logError.WriteLog(ex);
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("detail"))
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string  titleId = GridView1.DataKeys[index].Value.ToString();
                DataTable dt = (DataTable)Session["dataTable"];

                // Award Data Population
                BALAward balAward = new BALAward();
                AwardDetailsView.DataSource = balAward.getAward(titleId);
                AwardDetailsView.DataBind();

                // Participant Data Population
                BALParticipant balParticipant = new BALParticipant();
                ParticipantDetailsView.DataSource = balParticipant.getParticipant(titleId);
                ParticipantDetailsView.DataBind();

                // Storyline Data Population
                BALStoryline balStoryline = new BALStoryline();
                StoryLineDetailsView.DataSource = balStoryline.getStoryline(titleId);
                StoryLineDetailsView.DataBind();

                // Genre Data Population
                BALGenre balGenre = new BALGenre();
                GenreDetailsView.DataSource = balGenre.getGenre(titleId);
                GenreDetailsView.DataBind();

                // OtherName Data Population
                BALOtherName balOtherName = new BALOtherName();
                OthernameDetailsView.DataSource = balOtherName.getOtherName(titleId);
                OthernameDetailsView.DataBind();
                


                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#currentdetail').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(),
                           "ModalScript", sb.ToString(), false);

            }
        }

        // GridView.PageIndexChanging Event
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // Set the index of the new display page. 
            GridView1.PageIndex = e.NewPageIndex;

            // Rebind the GridView control to 
            // show  the data in the new page.
            BindGridView(null);
        }

       protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGridView(txtSearchName.Text);
        }

    }
}