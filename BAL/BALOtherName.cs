﻿using System;
using System.Collections;
using DAL;
using Entity;
using System.Collections.Generic;

namespace BAL
{
    public class BALOtherName
    {
        public List<OtherName> getOtherName(string titleId)
        {
            OtherNameData othernameData = new OtherNameData();
            return othernameData.getSelectedOtherName(titleId);
        }
    }
}
