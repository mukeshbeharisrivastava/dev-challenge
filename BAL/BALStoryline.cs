﻿using System;
using System.Collections;
using DAL;
using Entity;
using System.Collections.Generic;

namespace BAL
{
    public class BALStoryline
    {
        public List<StoryLine> getStoryline(string titleId)
        {
            StoryLineData storylineData = new StoryLineData();
            return storylineData.getSelectedStoryline(titleId);
        }
    }
}
