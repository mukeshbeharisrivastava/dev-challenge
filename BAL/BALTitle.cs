﻿using System;
using System.Collections;
using DAL;
using Entity;
using System.Collections.Generic;

namespace BAL
{
    public class BALTitle
    {
        public List<Title> getTitles(string titleName)
        {
            TitleData titleData  = new TitleData();

            if (string.IsNullOrEmpty(titleName))
            {
                return titleData.getAllTitle();
            }
            else
            {
                return titleData.getTitle(titleName);
            }
        }
    }
}
