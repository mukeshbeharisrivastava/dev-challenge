﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity;
using DAL;

namespace BAL
{
    public class BALGenre
    {
        public List<TitleGenre> getGenre(string titleId)
        {
            GenreData genreData = new GenreData();
            return genreData.getSelectedGenre(titleId);
        }
    }
}
