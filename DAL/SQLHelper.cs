﻿namespace DAL
{
    using System;
    using System.Data;
    using System.Data.SqlServerCe;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using MySql.Data.MySqlClient;
    using System.Data;
    using System.Configuration;

    public class SQLHelper
    {
        string connString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;

        public void executenonquery(List<SqlCeParameter> parameters, string SPName)
        {
            try
            {
                using (SqlCeConnection con = new SqlCeConnection(connString))
                {
                    using (SqlCeCommand cmd = new SqlCeCommand(SPName, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        foreach (SqlCeParameter param in parameters)
                        {
                            cmd.Parameters.Add(param);
                        }

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public string executeScaler(List<SqlCeParameter> parameters, string SPName)
        {
            try
            {
                using (SqlCeConnection con = new SqlCeConnection(connString))
                {
                    using (SqlCeCommand cmd = new SqlCeCommand(SPName, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        foreach (SqlCeParameter param in parameters)
                        {
                            cmd.Parameters.Add(param);
                        }

                        con.Open();
                        return Convert.ToString(cmd.ExecuteScalar());
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public T executeSP<T>(List<SqlCeParameter> parameters, string qry)
        {
            try
            {
                using (SqlCeConnection con = new SqlCeConnection(connString))
                {
                    using (SqlCeCommand cmd = new SqlCeCommand(qry, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        foreach (SqlCeParameter param in parameters)
                        {
                            cmd.Parameters.Add(param);
                        }

                        if (typeof(T) == typeof(DataSet))
                        {
                            SqlCeDataAdapter adapter = new SqlCeDataAdapter(cmd);
                            DataSet dset = new DataSet();
                            con.Open();
                            adapter.Fill(dset);
                            return (T)(object)dset;
                        }
                        else if (typeof(T) == typeof(int) || typeof(T) == typeof(string))
                        {
                            SqlCeParameter _ReturnValue = new SqlCeParameter("_ReturnValue", MySqlDbType.Int32);
                            _ReturnValue.Direction = System.Data.ParameterDirection.Output;
                            cmd.Parameters.Add(_ReturnValue);

                            con.Open();
                            cmd.ExecuteNonQuery();
                            return (T)_ReturnValue.Value;
                        }
                        else
                        {
                            return default(T);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

    }
}
