﻿using System.Collections.Generic;
using Entity;
using System.Data;
using System.Data.SqlServerCe;
using System;

namespace DAL
{
    public class OtherNameData
    {
        public List<OtherName> getSelectedOtherName(string titleId)
        {
            List<OtherName> otherNameList = new List<OtherName>();
            SQLHelper sqlHelper = new SQLHelper();
            List<SqlCeParameter> parameters = new List<SqlCeParameter>();
            string qry = 
                "SELECT OtherName.TitleId, OtherName.TitleNameLanguage, OtherName.TitleNameType, OtherName.TitleNameSortable, OtherName.TitleName, OtherName.Id " + 
                "FROM OtherName INNER JOIN Title ON OtherName.TitleId = Title.TitleId " +
                "where Title.TitleId ='" + titleId + "'";

            var resultSet = sqlHelper.executeSP<DataSet>(parameters, qry);

            OtherName otherName ;
            foreach (DataRow drow in resultSet.Tables[0].Rows)
            {
                otherName = new OtherName(Convert.ToInt32(drow[0].ToString()),
                                          drow[1].ToString(),
                                          drow[2].ToString(),
                                          drow[3].ToString(),
                                          drow[4].ToString(),
                                          Convert.ToInt32(drow[5].ToString()));
                otherNameList.Add(otherName);
            }

            return otherNameList;
        }
    }
}
