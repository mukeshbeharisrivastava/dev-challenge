﻿using System.Collections.Generic;
using Entity;
using System.Data;
using System.Data.SqlServerCe;
using System;

namespace DAL
{
    public class AwardData
    {
        public List<Award> getSelectedAward(string titleId)
        {
            List<Award> awardList = new List<Award>();
            SQLHelper sqlHelper = new SQLHelper();
            List<SqlCeParameter> parameters = new List<SqlCeParameter>();
            string qry = "SELECT * FROM Award where TitleId ='" + titleId + "'";
            var resultSet = sqlHelper.executeSP<DataSet>(parameters, qry);

            Award award ;
            foreach (DataRow drow in resultSet.Tables[0].Rows)
            {
                award = new Award(Convert.ToInt32(drow[0].ToString()),
                    Convert.ToBoolean(drow[1].ToString()),
                    Convert.ToInt32(drow[2].ToString()),
                    drow[3].ToString(),
                    drow[4].ToString(),
                    Convert.ToInt32(drow[5].ToString()));
                awardList.Add(award);
            }

            return awardList;
        }
    }
}
