﻿using System.Collections.Generic;
using Entity;
using System.Data;
using System.Data.SqlServerCe;
using System;

namespace DAL
{
    public class ParticipantData
    {
        public List<TitleParticipant> getSelectedParticipant(string titleId)
        {
            List<TitleParticipant> participantList = new List<TitleParticipant>();
            SQLHelper sqlHelper = new SQLHelper();
            List<SqlCeParameter> parameters = new List<SqlCeParameter>();
            string qry =
                    "SELECT Participant.Id, Participant.Name, Participant.ParticipantType,  " +
                    "TitleParticipant.IsKey, TitleParticipant.RoleType, TitleParticipant.IsOnScreen " +
                    "FROM Participant INNER JOIN " +
                    "TitleParticipant ON Participant.Id = TitleParticipant.ParticipantId INNER JOIN Title ON TitleParticipant.TitleId = Title.TitleId " +
                    "where Title.TitleId ='" + titleId + "'";
            var resultSet = sqlHelper.executeSP<DataSet>(parameters, qry);

            foreach (DataRow drow in resultSet.Tables[0].Rows)
            {
                TitleParticipant titleparticipant = new TitleParticipant();
                titleparticipant.Id = Convert.ToInt32(drow[0].ToString());
                titleparticipant.Name = drow[1].ToString();
                titleparticipant.ParticipantType = drow[2].ToString();
                titleparticipant.IsKey = Convert.ToBoolean(drow[3].ToString());
                titleparticipant.RoleType = drow[4].ToString();
                titleparticipant.IsOnScreen = Convert.ToBoolean(drow[5].ToString());

                participantList.Add(titleparticipant);
            }
            return participantList;
        }

    }
}
