﻿using System.Collections.Generic;
using Entity;
using System.Data;
using System.Data.SqlServerCe;
using System;

namespace DAL
{
    public class GenreData
    {
        public List<TitleGenre> getSelectedGenre(string titleId)
        {
            List<TitleGenre> genreList = new List<TitleGenre>();
            SQLHelper sqlHelper = new SQLHelper();
            List<SqlCeParameter> parameters = new List<SqlCeParameter>();
            string qry =
                        "SELECT Genre.Id, Genre.Name FROM Genre INNER JOIN " +
                         "TitleGenre ON Genre.Id = TitleGenre.GenreId INNER JOIN " +
                         "Title ON TitleGenre.TitleId = Title.TitleId " +
                         "where Title.TitleId ='" + titleId + "'";

            var resultSet = sqlHelper.executeSP<DataSet>(parameters, qry);

            foreach (DataRow drow in resultSet.Tables[0].Rows)
            {
                TitleGenre titleGenre = new TitleGenre();
                titleGenre.GenreId = Convert.ToInt32(drow[0].ToString());
                titleGenre.Name = drow[1].ToString();
                genreList.Add(titleGenre);
            }
            return genreList;
        }

    }
}
