﻿using System.Collections.Generic;
using Entity;
using System.Data;
using System.Data.SqlServerCe;
using System;

namespace DAL
{
    public class StoryLineData
    {
        public List<StoryLine> getSelectedStoryline(string titleId)
        {
            List<StoryLine> storylineList = new List<StoryLine>();
            SQLHelper sqlHelper = new SQLHelper();
            List<SqlCeParameter> parameters = new List<SqlCeParameter>();
            string qry = "SELECT * FROM Storyline where TitleId ='" + titleId + "'";
            var resultSet = sqlHelper.executeSP<DataSet>(parameters, qry);

            StoryLine storyline ;
            foreach (DataRow drow in resultSet.Tables[0].Rows)
            {
                storyline = new StoryLine(Convert.ToInt32(drow[0].ToString()),
                    drow[1].ToString(),
                    drow[2].ToString(),
                    drow[3].ToString(),
                    Convert.ToInt32(drow[4].ToString()));
                storylineList.Add(storyline);
            }

            return storylineList;
        }
    }
}
