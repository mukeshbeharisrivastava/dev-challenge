﻿using System.Collections.Generic;
using Entity;
using System.Data;
using System.Data.SqlServerCe;
using System;

namespace DAL
{
    public class TitleData
    {

        public List<Title> getAllTitle()
        {
            List<Title> titleList = new List<Title>();
            SQLHelper sqlHelper = new SQLHelper();
            List<SqlCeParameter> parameters = new List<SqlCeParameter>();
            var resultSet = sqlHelper.executeSP<DataSet>(parameters, "Select * from Title");


            Title title;
            foreach (DataRow drow in resultSet.Tables[0].Rows)
            {
                title = new Title(Convert.ToInt32(drow[0].ToString()),
                    drow[1].ToString(),
                    drow[2].ToString(),
                    drow[3] == DBNull.Value ? 0 : Convert.ToInt32(drow[3].ToString()),
                    Convert.ToInt32(drow[4].ToString()),
                    Convert.ToDateTime(drow[5].ToString()));
                titleList.Add(title);
            }

            return titleList;
        }

        public List<Title> getTitle(string searchTitle)
        {
            List<Title> titleList = new List<Title>();
            SQLHelper sqlHelper = new SQLHelper();
            List<SqlCeParameter> parameters = new List<SqlCeParameter>();
            string qry = "SELECT * FROM Title where TitleName like'%" + searchTitle + "%'";
            var resultSet = sqlHelper.executeSP<DataSet>(parameters, qry);

            Title title;
            foreach (DataRow drow in resultSet.Tables[0].Rows)
            {
                title = new Title(Convert.ToInt32(drow[0].ToString()),
                    drow[1] == DBNull.Value ? null : drow[1].ToString(),
                    drow[2] == DBNull.Value ? null : drow[2].ToString(),
                    drow[3] == DBNull.Value ? 0 : Convert.ToInt32(drow[3].ToString()),
                    drow[4] == DBNull.Value ? 0 : Convert.ToInt32(drow[4].ToString()),
                    drow[5] == DBNull.Value ? new DateTime() : Convert.ToDateTime(drow[5].ToString()));
                titleList.Add(title);
            }

            return titleList;
        }

    }
}
