﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class StoryLine
    {
        public int TitleId { get; set; }
        public int Id { get; set; }
        public string Type { get; set; }
        public string Language { get; set; }
        public string Description { get; set; }

        public StoryLine()
        { }

        public StoryLine(int titleId, string type, string language, string description, int id)
        {
            this.TitleId = titleId;
            this.Id = id;
            this.Type = type;
            this.Language = language;
            this.Description = description;
        }

    }
}
