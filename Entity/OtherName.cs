﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class OtherName
    {
        public int TitleId { get; set; }
        public int Id { get; set; }
        public string TitleNameLanguage { get; set; }
        public string TitleNameType { get; set; }
        public string TitleNameSortable { get; set; }
        public string TitleName { get; set; }

        public OtherName()
        { }

        public OtherName(int titleId, string titleNameLanguage, string titleNameType, string titleNameSortable, string titleName,int id)
        {
            this.TitleId = titleId;
            this.Id = id;
            this.TitleNameLanguage = titleNameLanguage;
            this.TitleNameType = titleNameType;
            this.TitleNameSortable = titleNameSortable;
            this.TitleName = titleName;
        }
    }
}
