﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class Genre
    {

        public int Id { get; set; }
        public string Name { get; set; }

        public Genre()
        { }

        public Genre(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
    }
}
