﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class TitleGenre : Genre
    {
        public int Id { get; set; }
        public int TitleId { get; set; }
        public int GenreId { get; set; }

        public TitleGenre()
        { }

        public TitleGenre(int id, int titleId, int genreId)
        {
            this.Id = id;
            this.TitleId = titleId;
            this.GenreId = genreId;
        }

    }
}
