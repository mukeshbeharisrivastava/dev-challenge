﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class Award
    {
        public int TitleId { get; set; }
        public int Id { get; set; }
        public bool AwardWon { get; set; }
        public int AwardYear { get; set; }
        public string AwardName { get; set; }
        public string AwardCompany { get; set; }

        public Award()
        { }

        public Award(int titleId, bool awardWon, int awardYear, string awardName, string awardCompany, int id)
        {
            this.TitleId = titleId;
            this.Id = id;
            this.AwardWon = awardWon;
            this.AwardYear = awardYear;
            this.AwardName = awardName;
            this.AwardCompany = awardCompany;
        }
    }
}
