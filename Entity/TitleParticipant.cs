﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class TitleParticipant :Participant
    {
        public int Id { get; set; }
        public int TitleId { get; set; }
        public int ParticipantId { get; set; }
        public bool IsKey { get; set; }
        public string RoleType { get; set; }
        public bool IsOnScreen { get; set; }

        public TitleParticipant()
        { }

        public TitleParticipant(int id, int titleId, int participantId, bool isKey,string roleType, bool isOnscreen)
        {
            this.Id = id;
            this.TitleId = titleId;
            this.ParticipantId = participantId;
            this.IsKey = IsKey;
            this.RoleType = roleType;
            this.IsOnScreen = isOnscreen;
        }

    }
}
