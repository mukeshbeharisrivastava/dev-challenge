﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class Participant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ParticipantType { get; set; }

        public Participant()
        { }

        public Participant(int id, string name, string participantType)
        {
            this.Id = id;
            this.Name = name;
            this.ParticipantType = participantType;
        }
    }
}
