﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class Title
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string TitleNameSortable { get; set; }
        public int TitleTypeId { get; set; }
        public int ReleaseYear { get; set; }
        public DateTime ProcessedDateTimeUTC { get; set; }

        public Title()
        {

        }

        public Title(int titleId, string titleName, string titleNameSortable, int titleTypeId, int releaseYear, DateTime processedDateTimeUTC)
        {
            this.TitleId = titleId;
            this.TitleName = titleName;
            this.TitleNameSortable = titleNameSortable;
            this.TitleTypeId = titleTypeId;
            this.ReleaseYear = releaseYear;
            this.ProcessedDateTimeUTC = processedDateTimeUTC;
        }
    }
}
