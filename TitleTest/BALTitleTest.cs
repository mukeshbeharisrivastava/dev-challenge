﻿using BAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Entity;
using System.Collections.Generic;

namespace TitleTest
{
    
    
    /// <summary>
    ///This is a test class for BALTitleTest and is intended
    ///to contain all BALTitleTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BALTitleTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BALTitle Constructor
        ///</summary>
        [TestMethod()]
        public void BALTitleConstructorTest()
        {
            BALTitle target = new BALTitle();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for getTitle
        ///</summary>
        [TestMethod()]
        public void getTitleTest()
        {
            BALTitle target = new BALTitle();
            string titleName = "Cavalcade";
            List<Title> titleList = new List<Title>();
            Title title = new Title(70523, "Cavalcade", "Cavalcade", 0, 1933, Convert.ToDateTime("6/15/2013 2:01:55 AM"));
            titleList.Add(title);

            List<Title> actual;
            actual = target.getTitles(titleName);
            Assert.IsNotNull(titleList);
            Assert.AreEqual(titleList[0].TitleId,actual[0].TitleId);
        }

        /// <summary>
        ///A test for getAllTitles
        ///</summary>
        [TestMethod()]
        public void getAllTitleTest()
        {
            BALTitle target = new BALTitle();
            List<Title> actual = target.getTitles(null);
            Assert.IsNotNull(actual);
            Assert.AreEqual(25,actual.Count);
        }
    }
}
